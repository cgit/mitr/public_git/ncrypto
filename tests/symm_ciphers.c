/* ncr_symm_cipher_* tests.

Copyright 2010 Red Hat, Inc.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Red Hat author: Miloslav Trmač <mitr@redhat.com> */

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <ncrypto/ncrypto.h>

struct tv
{
  CK_MECHANISM_TYPE mech;
  CK_MECHANISM_TYPE key_gen_mech;
  CK_KEY_TYPE key_type;
  const uint8_t *key;
  size_t key_size;
  const uint8_t *iv;
  size_t iv_size;
  const uint8_t *input;
  size_t input_size;
  const uint8_t *output;
  size_t output_size;
};

static const struct tv tvs[] =
  {
#define TV(M, GM, K, KEY, IV, IN, OUT)					\
    {									\
      (M), (GM), (K), (const uint8_t *)(KEY), sizeof (KEY) - 1,		\
      (const uint8_t *)(IV), sizeof (IV) - 1, (const uint8_t *)(IN),	\
    sizeof (IN) - 1, (const uint8_t *)(OUT), sizeof (OUT) - 1		\
    }
    TV (CKM_AES_ECB, CKM_AES_KEY_GEN, CKK_AES,
	"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F", "",
	"\x00\x11\x22\x33\x44\x55\x66\x77\x88\x99\xAA\xBB\xCC\xDD\xEE\xFF",
	"\x69\xC4\xE0\xD8\x6A\x7B\x04\x30\xD8\xCD\xB7\x80\x70\xB4\xC5\x5A"),
    TV (CKM_AES_ECB, CKM_AES_KEY_GEN, CKK_AES,
	"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17",
	"", "\x00\x11\x22\x33\x44\x55\x66\x77\x88\x99\xAA\xBB\xCC\xDD\xEE\xFF",
	"\xDD\xA9\x7C\xA4\x86\x4C\xDF\xE0\x6E\xAF\x70\xA0\xEC\x0D\x71\x91"),
    TV (CKM_AES_ECB, CKM_AES_KEY_GEN, CKK_AES,
	"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F",
	"", "\x00\x11\x22\x33\x44\x55\x66\x77\x88\x99\xAA\xBB\xCC\xDD\xEE\xFF",
	"\x8E\xA2\xB7\xCA\x51\x67\x45\xBF\xEA\xFC\x49\x90\x4B\x49\x60\x89"),

    TV (CKM_AES_CBC, CKM_AES_KEY_GEN, CKK_AES,
	"\x2B\x7E\x15\x16\x28\xAE\xD2\xA6\xAB\xF7\x15\x88\x09\xCF\x4F\x3C",
	"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F",
	"\x6B\xC1\xBE\xE2\x2E\x40\x9F\x96\xE9\x3D\x7E\x11\x73\x93\x17\x2A\xAE\x2D\x8A\x57\x1E\x03\xAC\x9C\x9E\xB7\x6F\xAC\x45\xAF\x8E\x51\x30\xC8\x1C\x46\xA3\x5C\xE4\x11\xE5\xFB\xC1\x19\x1A\x0A\x52\xEF\xF6\x9F\x24\x45\xDF\x4F\x9B\x17\xAD\x2B\x41\x7B\xE6\x6C\x37\x10",
	"\x76\x49\xAB\xAC\x81\x19\xB2\x46\xCE\xE9\x8E\x9B\x12\xE9\x19\x7D\x50\x86\xCB\x9B\x50\x72\x19\xEE\x95\xDB\x11\x3A\x91\x76\x78\xB2\x73\xBE\xD6\xB8\xE3\xC1\x74\x3B\x71\x16\xE6\x9E\x22\x22\x95\x16\x3F\xF1\xCA\xA1\x68\x1F\xAC\x09\x12\x0E\xCA\x30\x75\x86\xE1\xA7"),
    TV (CKM_AES_CBC, CKM_AES_KEY_GEN, CKK_AES,
	"\x8E\x73\xB0\xF7\xDA\x0E\x64\x52\xC8\x10\xF3\x2B\x80\x90\x79\xE5\x62\xF8\xEA\xD2\x52\x2C\x6B\x7B",
	"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F",
	"\x6B\xC1\xBE\xE2\x2E\x40\x9F\x96\xE9\x3D\x7E\x11\x73\x93\x17\x2A\xAE\x2D\x8A\x57\x1E\x03\xAC\x9C\x9E\xB7\x6F\xAC\x45\xAF\x8E\x51\x30\xC8\x1C\x46\xA3\x5C\xE4\x11\xE5\xFB\xC1\x19\x1A\x0A\x52\xEF\xF6\x9F\x24\x45\xDF\x4F\x9B\x17\xAD\x2B\x41\x7B\xE6\x6C\x37\x10",
	"\x4F\x02\x1D\xB2\x43\xBC\x63\x3D\x71\x78\x18\x3A\x9F\xA0\x71\xE8\xB4\xD9\xAD\xA9\xAD\x7D\xED\xF4\xE5\xE7\x38\x76\x3F\x69\x14\x5A\x57\x1B\x24\x20\x12\xFB\x7A\xE0\x7F\xA9\xBA\xAC\x3D\xF1\x02\xE0\x08\xB0\xE2\x79\x88\x59\x88\x81\xD9\x20\xA9\xE6\x4F\x56\x15\xCD"),
    TV (CKM_AES_CBC, CKM_AES_KEY_GEN, CKK_AES,
	"\x60\x3D\xEB\x10\x15\xCA\x71\xBE\x2B\x73\xAE\xF0\x85\x7D\x77\x81\x1F\x35\x2C\x07\x3B\x61\x08\xD7\x2D\x98\x10\xA3\x09\x14\xDF\xF4",
	"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F",
	"\x6B\xC1\xBE\xE2\x2E\x40\x9F\x96\xE9\x3D\x7E\x11\x73\x93\x17\x2A\xAE\x2D\x8A\x57\x1E\x03\xAC\x9C\x9E\xB7\x6F\xAC\x45\xAF\x8E\x51\x30\xC8\x1C\x46\xA3\x5C\xE4\x11\xE5\xFB\xC1\x19\x1A\x0A\x52\xEF\xF6\x9F\x24\x45\xDF\x4F\x9B\x17\xAD\x2B\x41\x7B\xE6\x6C\x37\x10",
	"\xF5\x8C\x4C\x04\xD6\xE5\xF1\xBA\x77\x9E\xAB\xFB\x5F\x7B\xFB\xD6\x9C\xFC\x4E\x96\x7E\xDB\x80\x8D\x67\x9F\x77\x7B\xC6\x70\x2C\x7D\x39\xF2\x33\x69\xA9\xD9\xBA\xCF\xA5\x30\xE2\x63\x04\x23\x14\x61\xB2\xEB\x05\xE2\xC3\x9B\xE9\xFC\xDA\x6C\x19\x07\x8C\x6A\x9D\x1B"),

    TV (CKM_DES3_ECB, CKM_DES3_KEY_GEN, CKK_DES3,
	"\x01\x23\x45\x67\x89\xAB\xCD\xEF\x23\x45\x67\x89\xAB\xCD\xEF\x01\x45\x67\x89\xAB\xCD\xEF\x01\x23",
	"",
	/* It is really "qufck" - the example in SP 800-67 says "quick", but
	   the hexadecimal version is different. */
	"The qufck brown fox jump",
	"\xA8\x26\xFD\x8C\xE5\x3B\x85\x5F\xCC\xE2\x1C\x81\x12\x25\x6F\xE6\x68\xD5\xC0\x5D\xD9\xB6\xB9\x00"),
#undef TV
  };

#define LARGE_SIZE (10 * 1024 * 1024)

int
main (void)
{
  struct ncr_symm_cipher_session *sess;
  struct ncr_symm_key *key;
  uint8_t dest[256];
  size_t i, j, dest_size;
  void *large_src, *large_dest;
  CK_RV res;

  /* Test init + update + final. */
  for (i = 0; i < G_N_ELEMENTS (tvs); i++)
    {
      res = ncr_symm_cipher_alloc (&sess, tvs[i].mech);
      assert (res == CKR_OK);

      res = ncr_symm_key_create (&key, tvs[i].key_type, true, tvs[i].key,
				 tvs[i].key_size);
      assert (res == CKR_OK);

      for (j = 0; j < 2; j++)
	{
	  res = ncr_symm_cipher_encrypt_init (sess, key, tvs[i].iv,
					      tvs[i].iv_size);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_encrypt_update (sess, dest, &dest_size,
						tvs[i].input,
						tvs[i].input_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].output_size);
	  assert (memcmp (dest, tvs[i].output, dest_size) == 0);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_encrypt_final (sess, dest, &dest_size,
					       NULL, 0);
	  assert (res == CKR_OK);
	  assert (dest_size == 0);

	  res = ncr_symm_cipher_decrypt_init (sess, key, tvs[i].iv,
					      tvs[i].iv_size);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_decrypt_update (sess, dest, &dest_size,
						tvs[i].output,
						tvs[i].output_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].input_size);
	  assert (memcmp (dest, tvs[i].input, dest_size) == 0);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_decrypt_final (sess, dest, &dest_size,
					       NULL, 0);
	  assert (res == CKR_OK);
	  assert (dest_size == 0);
	}

      res = ncr_symm_key_destroy (key);
      assert (res == CKR_OK);

      res = ncr_symm_cipher_free (sess);
      assert (res == CKR_OK);
    }

  /* Test init + {en,de}crypt. */
  for (i = 0; i < G_N_ELEMENTS (tvs); i++)
    {
      res = ncr_symm_cipher_alloc (&sess, tvs[i].mech);
      assert (res == CKR_OK);

      res = ncr_symm_key_create (&key, tvs[i].key_type, true, tvs[i].key,
				 tvs[i].key_size);
      assert (res == CKR_OK);

      for (j = 0; j < 2; j++)
	{
	  res = ncr_symm_cipher_encrypt_init (sess, key, tvs[i].iv,
					      tvs[i].iv_size);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_encrypt (sess, dest, &dest_size, tvs[i].input,
					 tvs[i].input_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].output_size);
	  assert (memcmp (dest, tvs[i].output, dest_size) == 0);

	  res = ncr_symm_cipher_decrypt_init (sess, key, tvs[i].iv,
					      tvs[i].iv_size);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_decrypt (sess, dest, &dest_size, tvs[i].output,
					 tvs[i].output_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].input_size);
	  assert (memcmp (dest, tvs[i].input, dest_size) == 0);
	}

      res = ncr_symm_key_destroy (key);
      assert (res == CKR_OK);

      res = ncr_symm_cipher_free (sess);
      assert (res == CKR_OK);
    }

  /* Test changing of the IV. */
  for (i = 0; i < G_N_ELEMENTS (tvs); i++)
    {
      res = ncr_symm_cipher_alloc (&sess, tvs[i].mech);
      assert (res == CKR_OK);

      res = ncr_symm_key_create (&key, tvs[i].key_type, true, tvs[i].key,
				 tvs[i].key_size);
      assert (res == CKR_OK);

      res = ncr_symm_cipher_encrypt_init (sess, key, tvs[i].iv, tvs[i].iv_size);
      assert (res == CKR_OK);

      dest_size = sizeof (dest);
      res = ncr_symm_cipher_encrypt_update (sess, dest, &dest_size,
					    tvs[i].input, tvs[i].input_size);
      assert (res == CKR_OK);
      assert (dest_size == tvs[i].output_size);
      assert (memcmp (dest, tvs[i].output, dest_size) == 0);

      res = ncr_symm_cipher_change_iv (sess, tvs[i].iv, tvs[i].iv_size);
      assert (res == CKR_OK);

      dest_size = sizeof (dest);
      res = ncr_symm_cipher_encrypt_final (sess, dest, &dest_size, tvs[i].input,
					   tvs[i].input_size);
      assert (res == CKR_OK);
      assert (dest_size == tvs[i].output_size);
      assert (memcmp (dest, tvs[i].output, dest_size) == 0);

      res = ncr_symm_cipher_decrypt_init (sess, key, tvs[i].iv, tvs[i].iv_size);
      assert (res == CKR_OK);

      dest_size = sizeof (dest);
      res = ncr_symm_cipher_decrypt_update (sess, dest, &dest_size,
					    tvs[i].output, tvs[i].output_size);
      assert (res == CKR_OK);
      assert (dest_size == tvs[i].input_size);
      assert (memcmp (dest, tvs[i].input, dest_size) == 0);

      res = ncr_symm_cipher_change_iv (sess, tvs[i].iv, tvs[i].iv_size);
      assert (res == CKR_OK);

      dest_size = sizeof (dest);
      res = ncr_symm_cipher_decrypt_final (sess, dest, &dest_size,
					   tvs[i].output, tvs[i].output_size);
      assert (res == CKR_OK);
      assert (dest_size == tvs[i].input_size);
      assert (memcmp (dest, tvs[i].input, dest_size) == 0);

      res = ncr_symm_key_destroy (key);
      assert (res == CKR_OK);

      res = ncr_symm_cipher_free (sess);
      assert (res == CKR_OK);
    }

  /* Test init + update + final with a random key. */
  for (i = 0; i < G_N_ELEMENTS (tvs); i++)
    {
      res = ncr_symm_cipher_alloc (&sess, tvs[i].mech);
      assert (res == CKR_OK);

      res = ncr_symm_key_generate (&key, tvs[i].key_gen_mech, true,
				   tvs[i].key_size);
      assert (res == CKR_OK);

      for (j = 0; j < 2; j++)
	{
	  res = ncr_symm_cipher_encrypt_init (sess, key, tvs[i].iv,
					      tvs[i].iv_size);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_encrypt_update (sess, dest, &dest_size,
						tvs[i].input,
						tvs[i].input_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].output_size);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_encrypt_final (sess, dest, &dest_size,
					       NULL, 0);
	  assert (res == CKR_OK);
	  assert (dest_size == 0);

	  res = ncr_symm_cipher_decrypt_init (sess, key, tvs[i].iv,
					      tvs[i].iv_size);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_decrypt_update (sess, dest, &dest_size, dest,
						tvs[i].output_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].input_size);
	  assert (memcmp (dest, tvs[i].input, dest_size) == 0);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_decrypt_final (sess, dest, &dest_size,
					       NULL, 0);
	  assert (res == CKR_OK);
	  assert (dest_size == 0);
	}

      res = ncr_symm_key_destroy (key);
      assert (res == CKR_OK);

      res = ncr_symm_cipher_free (sess);
      assert (res == CKR_OK);
    }

  /* Test init + {en,de}crypt with a random key. */
  for (i = 0; i < G_N_ELEMENTS (tvs); i++)
    {
      res = ncr_symm_cipher_alloc (&sess, tvs[i].mech);
      assert (res == CKR_OK);

      res = ncr_symm_key_generate (&key, tvs[i].key_gen_mech, true,
				   tvs[i].key_size);
      assert (res == CKR_OK);

      for (j = 0; j < 2; j++)
	{
	  res = ncr_symm_cipher_encrypt_init (sess, key, tvs[i].iv,
					      tvs[i].iv_size);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_encrypt (sess, dest, &dest_size, tvs[i].input,
					 tvs[i].input_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].output_size);

	  res = ncr_symm_cipher_decrypt_init (sess, key, tvs[i].iv,
					      tvs[i].iv_size);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_cipher_decrypt (sess, dest, &dest_size, dest,
					 tvs[i].output_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].input_size);
	  assert (memcmp (dest, tvs[i].input, dest_size) == 0);
	}

      res = ncr_symm_key_destroy (key);
      assert (res == CKR_OK);

      res = ncr_symm_cipher_free (sess);
      assert (res == CKR_OK);
    }

  /* Test very large input. */
  res = ncr_symm_cipher_alloc (&sess, CKM_AES_CBC);
  assert (res == CKR_OK);

  res = ncr_symm_key_create (&key, CKK_AES, true,
			     "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F",
			     16);
  assert (res == CKR_OK);

  res = ncr_symm_cipher_encrypt_init (sess, key,
				      "\x00\x11\x22\x33\x44\x55\x66\x77\x88\x99\xAA\xBB\xCC\xDD\xEE\xFF",
				      16);
  assert (res == CKR_OK);

  large_src = g_malloc0 (LARGE_SIZE);
  for (i = 0; i < LARGE_SIZE / sizeof (i); i++)
    memcpy ((unsigned char *)large_src + i * sizeof (i), &i, sizeof (i));

  large_dest = g_malloc (LARGE_SIZE);
  dest_size = LARGE_SIZE;
  res = ncr_symm_cipher_encrypt (sess, large_dest, &dest_size, large_src,
				 LARGE_SIZE);
  assert (res == CKR_OK);
  assert (dest_size == LARGE_SIZE);

  res = ncr_symm_cipher_decrypt_init (sess, key,
				      "\x00\x11\x22\x33\x44\x55\x66\x77\x88\x99\xAA\xBB\xCC\xDD\xEE\xFF",
				      16);
  assert (res == CKR_OK);

  dest_size = LARGE_SIZE;
  res = ncr_symm_cipher_decrypt (sess, large_dest, &dest_size, large_dest,
				 LARGE_SIZE);
  assert (res == CKR_OK);
  assert (dest_size == LARGE_SIZE);
  assert (memcmp (large_src, large_dest, LARGE_SIZE) == 0);
  g_free (large_dest);
  g_free (large_src);

  res = ncr_symm_key_destroy (key);
  assert (res == CKR_OK);

  res = ncr_symm_cipher_free (sess);
  assert (res == CKR_OK);

  /* Close the implicit reference, primarily to shut up valgrind. */
  res = ncr_close ();
  assert (res == CKR_OK);

  return EXIT_SUCCESS;
}
