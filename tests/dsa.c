/* DSA tests.

Copyright 2010 Red Hat, Inc.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Red Hat author: Miloslav Trmač <mitr@redhat.com> */

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <ncrypto/ncrypto.h>

#define MPI_PRIME "\xD1\x58\x93\x91\x4F\x33\x5C\xB1\x44\xD0\x5E\x44\x5D\xD8\xC6\x8A\xD8\x48\x0F\x18\xF4\x9C\x58\xDB\x65\x9E\x24\x75\x45\x59\x83\xF5\xA4\x67\x2B\x63\xF6\x14\xFE\x2F\xE0\x1E\x2E\x32\x1E\xA0\xAA\xFB\x62\x4C\xEE\xFF\xBF\x3A\x39\x05\x04\x6A\xFF\xF8\x7F\x2B\x3C\x56\xD4\x56\x61\x3E\xB9\x29\x26\x43\x20\x71\xA4\xD3\x48\x30\x9E\xC3\x61\x0C\xDB\x41\x00\xCF\xFF\x48\x33\xB0\xB3\xA4\xCE\x9B\x05\x25\x7C\xF3\x66\x40\x00\x3E\x39\x7C\x74\x86\x72\xA9\xE8\x4F\xE6\xD9\x7B\x48\x0E\x13\xAC\x39\xE4\xD8\x3E\x71\x92\x51\xD5\xA0\x5C\x01"
#define MPI_SUBPRIME "\x96\x30\x8F\xD6\x53\x32\xDB\x77\x02\xFC\x78\x57\xF8\x5E\xA0\x4C\x34\xCC\xF3\xC5"
#define MPI_BASE "\x89\x85\x3F\x3C\x68\xE1\x8D\x1A\xEA\xE9\x96\x5B\xF5\x64\x64\x82\xEC\xC4\x13\xFC\xD3\xDA\xB8\xE9\xEB\x81\x96\x82\xFE\x2E\xF7\xA1\x91\x20\x01\xAF\x76\xB7\xA8\x0C\x30\x14\x6D\x3C\x31\xDA\x30\x41\x60\xE8\x5E\x0F\x1B\xD4\x58\xA0\xD3\xC9\x0F\x36\x93\xB4\xFB\x93\x69\xB3\xFF\xD1\x96\x40\x9D\xC8\x2E\x89\x5A\x5C\xB2\xD0\x6B\x43\xCE\x0E\x2D\x03\x79\x39\xA4\x3C\xE9\x84\xDB\x78\x93\x81\x28\xB3\x75\x82\x39\xC3\xF1\x83\x18\x29\x25\x14\xDF\xD2\x94\xFC\x1B\x42\x7E\x2B\x7F\x16\x3B\xDD\x80\xB1\x5E\xB4\x3C\xE6\x9A\xA0\x3F\x0F"

static const struct ncr_mpi generate_mpis[NCR_DSA_GEN_NUM_MPIS] =
  {
#define INT(NAME, VAL)							\
    [NCR_DSA_GEN_MPI_##NAME] = { (void *)(VAL), sizeof (VAL) - 1 }

    INT (PRIME, MPI_PRIME), INT (SUBPRIME, MPI_SUBPRIME), INT (BASE, MPI_BASE)
#undef INT
  };

static const struct ncr_mpi public_import_mpis[NCR_DSA_PUBLIC_NUM_MPIS] =
  {
#define INT(NAME, VAL)							\
    [NCR_DSA_PUBLIC_MPI_##NAME] = { (void *)(VAL), sizeof (VAL) - 1 }

    INT (PRIME, MPI_PRIME), INT (SUBPRIME, MPI_SUBPRIME), INT (BASE, MPI_BASE),
    INT (VALUE, "\x0D\x91\xC1\x82\xC1\x3C\x1B\x18\xBC\x98\xFE\x67\x23\x74\x2E\xD4\x5F\xA9\xFE\x55\x56\x44\x77\x2D\x72\x50\xF7\x68\x8F\xBD\x9A\xB1\x3F\xE0\x34\x12\xCB\xE8\x1E\x5E\xC2\x13\xD2\xB2\xBC\xFC\x55\xAC\x86\x83\x59\x7F\xCD\xEA\x99\x40\xA3\xB7\xC5\x9A\xA7\x7E\xD5\xEC\x0A\xC5\x8C\xD6\x36\x38\x62\xAE\x0B\x5D\xD1\x73\xCE\x33\x4D\x96\xA1\xE2\xB9\x54\x6B\xDB\xED\xE3\x2C\x07\x03\xC7\x5A\x36\x24\x69\xA8\x51\xD5\xD9\xF7\x2E\x2F\x56\xB7\xBD\x16\x5C\x53\x5F\xAC\x90\xE6\xC1\x7A\x58\xFC\x69\x9F\xAA\xEB\xB3\x24\xEC\xB7\x42\x8E\x8C")
#undef INT
  };

static const struct ncr_mpi private_import_mpis[NCR_DSA_PRIVATE_NUM_MPIS] =
  {
#define INT(NAME, VAL)							\
    [NCR_DSA_PRIVATE_MPI_##NAME] = { (void *)(VAL), sizeof (VAL) - 1 }

    INT (PRIME, MPI_PRIME), INT (SUBPRIME, MPI_SUBPRIME), INT (BASE, MPI_BASE),
    INT (VALUE, "\x3E\x72\xDF\x63\xA9\xE0\x3A\x30\xE5\x3E\x1E\x7D\x53\xAD\x8D\x51\xC7\xB2\x2B\x33")
#undef INT
  };

static const uint8_t input[20] = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13";

static void
validate_mpis (const struct ncr_mpi *a, const struct ncr_mpi *b,
	       size_t num_mpis)
{
  size_t i;

  for (i = 0; i < num_mpis; i++)
    assert (a[i].size == b[i].size);
  for (i = 0; i < num_mpis; i++)
    assert (memcmp (a[i].data, b[i].data, a[i].size) == 0);
}

static void
test_key_pair (struct ncr_public_key *public, struct ncr_private_key *private)
{
  uint8_t dest[4096];
  size_t dest_size;
  CK_RV res;

  /* Test signatures */
  dest_size = sizeof (dest);
  res = ncr_private_key_sign (CKM_DSA, private, dest, &dest_size, input,
			      sizeof (input));
  assert (res == CKR_OK);
  res = ncr_public_key_verify (CKM_DSA, public, dest, dest_size, input,
			       sizeof (input));
  assert (res == CKR_OK);
  dest[0]++;
  res = ncr_public_key_verify (CKM_DSA, public, dest, dest_size, input,
			       sizeof (input));
  assert (res != CKR_OK);
}

int
main (void)
{
  struct ncr_public_key *public;
  struct ncr_private_key *private;
  struct ncr_mpi dest_mpis[MAX ((int)NCR_DSA_PUBLIC_NUM_MPIS,
				(int)NCR_DSA_PRIVATE_NUM_MPIS)];
  uint8_t mpi_dest[MAX ((int)NCR_DSA_PUBLIC_NUM_MPIS,
			(int)NCR_DSA_PRIVATE_NUM_MPIS)][256];
  size_t i;
  CK_RV res;

  /* Test key loading.  Should we test the generic version as well? */
  res = ncr_public_key_create_dsa (&public, public_import_mpis);
  assert (res == CKR_OK);
  res = ncr_private_key_create_dsa
    (&private, false, private_import_mpis,
     &public_import_mpis[NCR_DSA_PUBLIC_MPI_VALUE]);
  assert (res == CKR_OK);

  test_key_pair (public, private);

  /* Test key export. */
  for (i = 0; i < NCR_DSA_PUBLIC_NUM_MPIS; i++)
    {
      dest_mpis[i].data = mpi_dest[i];
      dest_mpis[i].size = sizeof (mpi_dest[i]);
    }
  res = ncr_public_key_export_dsa (public, dest_mpis);
  assert (res == CKR_OK);
  validate_mpis (dest_mpis, public_import_mpis, NCR_DSA_PUBLIC_NUM_MPIS);

  for (i = 0; i < NCR_DSA_PRIVATE_NUM_MPIS; i++)
    {
      dest_mpis[i].data = mpi_dest[i];
      dest_mpis[i].size = sizeof (mpi_dest[i]);
    }
  res = ncr_private_key_export_dsa (private, dest_mpis);
  assert (res == CKR_OK);
  validate_mpis (dest_mpis, private_import_mpis, NCR_DSA_PRIVATE_NUM_MPIS);

  res = ncr_private_key_destroy (private);
  assert (res == CKR_OK);

  res = ncr_public_key_destroy (public);
  assert (res == CKR_OK);

  /* Test key generation. */
  res = ncr_key_pair_generate_dsa (&public, &private, CKM_DSA_KEY_PAIR_GEN,
				   false, generate_mpis);
  assert (res == CKR_OK);

  test_key_pair (public, private);

  /* Test key export - only test that it succeeds. */
  for (i = 0; i < NCR_DSA_PUBLIC_NUM_MPIS; i++)
    {
      dest_mpis[i].data = mpi_dest[i];
      dest_mpis[i].size = sizeof (mpi_dest[i]);
    }
  res = ncr_public_key_export_dsa (public, dest_mpis);
  assert (res == CKR_OK);

  for (i = 0; i < NCR_DSA_PRIVATE_NUM_MPIS; i++)
    {
      dest_mpis[i].data = mpi_dest[i];
      dest_mpis[i].size = sizeof (mpi_dest[i]);
    }
  res = ncr_private_key_export_dsa (private, dest_mpis);
  assert (res == CKR_OK);

  res = ncr_private_key_destroy (private);
  assert (res == CKR_OK);

  res = ncr_public_key_destroy (public);
  assert (res == CKR_OK);

  /* Test reference counting */
  res = ncr_open ();
  assert (res == CKR_OK);
  res = ncr_close ();
  assert (res == CKR_OK);
  /* Close the implicit reference */
  res = ncr_close ();
  assert (res == CKR_OK);
  /* Check further closes are invalid */
  res = ncr_close ();
  assert (res == CKR_GENERAL_ERROR);

  return EXIT_SUCCESS;
}
