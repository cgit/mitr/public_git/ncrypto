/* ncr_symm_signature_* tests.

Copyright 2010 Red Hat, Inc.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Red Hat author: Miloslav Trmač <mitr@redhat.com> */

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <ncrypto/ncrypto.h>

struct tv
{
  CK_MECHANISM_TYPE mech;
  const uint8_t *input;
  size_t input_size;
  const uint8_t *key;
  size_t key_size;
  const uint8_t *output;
  size_t output_size;
};

static const struct tv tvs[] =
  {
#define TV(M, IN, KEY, OUT)						\
    {									\
      (M), (const uint8_t *)(IN), sizeof (IN) - 1,			\
      (const uint8_t *)(KEY), sizeof (KEY) - 1, (const uint8_t *)(OUT),	\
      sizeof (OUT) - 1							\
    }
    TV (CKM_SHA_1_HMAC, "Sample #1",
	"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2A\x2B\x2C\x2D\x2E\x2F\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3A\x3B\x3C\x3D\x3E\x3F",
	"\x4F\x4C\xA3\xD5\xD6\x8B\xA7\xCC\x0A\x12\x08\xC9\xC6\x1E\x9C\x5D\xA0\x40\x3C\x0A"),
    TV (CKM_SHA_1_HMAC, "Sample #2",
	"\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3A\x3B\x3C\x3D\x3E\x3F\x40\x41\x42\x43",
	"\x09\x22\xD3\x40\x5F\xAA\x3D\x19\x4F\x82\xA4\x58\x30\x73\x7D\x5C\xC6\xC7\x5D\x24"),
    TV (CKM_SHA_1_HMAC, "Sample #3",
	"\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5A\x5B\x5C\x5D\x5E\x5F\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7A\x7B\x7C\x7D\x7E\x7F\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8A\x8B\x8C\x8D\x8E\x8F\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9A\x9B\x9C\x9D\x9E\x9F\xA0\xA1\xA2\xA3\xA4\xA5\xA6\xA7\xA8\xA9\xAA\xAB\xAC\xAD\xAE\xAF\xB0\xB1\xB2\xB3",
	"\xBC\xF4\x1E\xAB\x8B\xB2\xD8\x02\xF3\xD0\x5C\xAF\x7C\xB0\x92\xEC\xF8\xD1\xA3\xAA"),
    TV (CKM_SHA_1_HMAC, "Sample #4",
	"\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7A\x7B\x7C\x7D\x7E\x7F\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8A\x8B\x8C\x8D\x8E\x8F\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9A\x9B\x9C\x9D\x9E\x9F\xA0",
	"\x9E\xA8\x86\xEF\xE2\x68\xDB\xEC\xCE\x42\x0C\x75\x24\xDF\x32\xE0\x75\x1A\x2A\x26")
#undef TV
  };

int
main (void)
{
  struct ncr_symm_signature_session *sess;
  struct ncr_symm_key *key;
  uint8_t dest[256];
  size_t i, j, k, dest_size;
  CK_RV res;

  /* Test init + update + final. */
  for (i = 0; i < G_N_ELEMENTS (tvs); i++)
    {
      res = ncr_symm_signature_alloc (&sess, tvs[i].mech);
      assert (res == CKR_OK);

      res = ncr_symm_key_create (&key, CKK_GENERIC_SECRET, true, tvs[i].key,
				 tvs[i].key_size);
      assert (res == CKR_OK);

      for (j = 0; j < 2; j++)
	{
	  res = ncr_symm_signature_sign_init (sess, key);
	  assert (res == CKR_OK);

	  res = ncr_symm_signature_sign_update (sess, tvs[i].input,
						tvs[i].input_size);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_signature_sign_final (sess, dest, &dest_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].output_size);
	  assert (memcmp (dest, tvs[i].output, dest_size) == 0);

	  for (k = 0; k < 2; k++)
	    {
	      res = ncr_symm_signature_verify_init (sess, key);
	      assert (res == CKR_OK);

	      res = ncr_symm_signature_verify_update (sess, tvs[i].input,
						      tvs[i].input_size);
	      assert (res == CKR_OK);

	      memcpy (dest, tvs[i].output, tvs[i].output_size);
	      dest[0] += k;
	      res = ncr_symm_signature_verify_final (sess, dest,
						     tvs[i].output_size);
	      if (k == 0)
		assert (res == CKR_OK);
	      else
		assert (res == CKR_SIGNATURE_INVALID);
	    }
	}

      res = ncr_symm_key_destroy (key);
      assert (res == CKR_OK);

      res = ncr_symm_signature_free (sess);
      assert (res == CKR_OK);
    }

  /* Test session cloning. */
  for (i = 0; i < G_N_ELEMENTS (tvs); i++)
    {
      res = ncr_symm_signature_alloc (&sess, tvs[i].mech);
      assert (res == CKR_OK);

      res = ncr_symm_key_create (&key, CKK_GENERIC_SECRET, true, tvs[i].key,
				 tvs[i].key_size);
      assert (res == CKR_OK);

      for (j = 0; j < 2; j++)
	{
	  struct ncr_symm_signature_session *clone;

	  res = ncr_symm_signature_sign_init (sess, key);
	  assert (res == CKR_OK);

	  res = ncr_symm_signature_sign_update (sess, tvs[i].input,
						tvs[i].input_size / 2);
	  assert (res == CKR_OK);

	  res = ncr_symm_signature_clone (&clone, sess);
	  assert (res == CKR_OK);

	  res = ncr_symm_signature_sign_update (sess, "", 1);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_signature_sign_final (sess, dest, &dest_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].output_size);
	  assert (memcmp (dest, tvs[i].output, dest_size) != 0);

	  res = ncr_symm_signature_sign_update (clone, tvs[i].input
						+ tvs[i].input_size / 2,
						tvs[i].input_size
						- tvs[i].input_size / 2);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_signature_sign_final (clone, dest, &dest_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].output_size);
	  assert (memcmp (dest, tvs[i].output, dest_size) == 0);

	  res = ncr_symm_signature_free (clone);
	  assert (res == CKR_OK);

	  for (k = 0; k < 2; k++)
	    {
	      res = ncr_symm_signature_verify_init (sess, key);
	      assert (res == CKR_OK);

	      res = ncr_symm_signature_verify_update (sess, tvs[i].input,
						      tvs[i].input_size / 2);
	      assert (res == CKR_OK);

	      res = ncr_symm_signature_clone (&clone, sess);
	      assert (res == CKR_OK);

	      res = ncr_symm_signature_verify_update (sess, "", 1);
	      assert (res == CKR_OK);

	      res = ncr_symm_signature_verify_final (sess, tvs[i].output,
						     tvs[i].output_size);
	      assert (res == CKR_SIGNATURE_INVALID);

	      res = ncr_symm_signature_verify_update (clone, tvs[i].input
						      + tvs[i].input_size / 2,
						      tvs[i].input_size
						      - tvs[i].input_size / 2);
	      assert (res == CKR_OK);

	      memcpy (dest, tvs[i].output, tvs[i].output_size);
	      dest[0] += k;
	      res = ncr_symm_signature_verify_final (clone, dest,
						     tvs[i].output_size);
	      if (k == 0)
		assert (res == CKR_OK);
	      else
		assert (res == CKR_SIGNATURE_INVALID);

	      res = ncr_symm_signature_free (clone);
	      assert (res == CKR_OK);
	    }
	}

      res = ncr_symm_key_destroy (key);
      assert (res == CKR_OK);

      res = ncr_symm_signature_free (sess);
      assert (res == CKR_OK);
    }

  /* Test init + {sign,verify}. */
  for (i = 0; i < G_N_ELEMENTS (tvs); i++)
    {
      res = ncr_symm_signature_alloc (&sess, tvs[i].mech);
      assert (res == CKR_OK);

      res = ncr_symm_key_create (&key, CKK_GENERIC_SECRET, true, tvs[i].key,
				 tvs[i].key_size);
      assert (res == CKR_OK);

      for (j = 0; j < 2; j++)
	{
	  res = ncr_symm_signature_sign_init (sess, key);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_signature_sign (sess, dest, &dest_size,
					 tvs[i].input, tvs[i].input_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].output_size);
	  assert (memcmp (dest, tvs[i].output, dest_size) == 0);

	  for (k = 0; k < 2; k++)
	    {
	      res = ncr_symm_signature_verify_init (sess, key);
	      assert (res == CKR_OK);

	      memcpy (dest, tvs[i].output, tvs[i].output_size);
	      dest[0] += k;
	      res = ncr_symm_signature_verify (sess, dest, tvs[i].output_size,
					       tvs[i].input, tvs[i].input_size);
	      if (k == 0)
		assert (res == CKR_OK);
	      else
		assert (res == CKR_SIGNATURE_INVALID);
	    }
	}

      res = ncr_symm_key_destroy (key);
      assert (res == CKR_OK);

      res = ncr_symm_signature_free (sess);
      assert (res == CKR_OK);
    }

  /* Test init + update + final with a random key. */
  for (i = 0; i < G_N_ELEMENTS (tvs); i++)
    {
      res = ncr_symm_signature_alloc (&sess, tvs[i].mech);
      assert (res == CKR_OK);

      res = ncr_symm_key_generate (&key, CKM_GENERIC_SECRET_KEY_GEN, true,
				   tvs[i].key_size);
      assert (res == CKR_OK);

      for (j = 0; j < 2; j++)
	{
	  res = ncr_symm_signature_sign_init (sess, key);
	  assert (res == CKR_OK);

	  res = ncr_symm_signature_sign_update (sess, tvs[i].input,
						tvs[i].input_size);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_signature_sign_final (sess, dest, &dest_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].output_size);

	  for (k = 0; k < 2; k++)
	    {
	      res = ncr_symm_signature_verify_init (sess, key);
	      assert (res == CKR_OK);

	      res = ncr_symm_signature_verify_update (sess, tvs[i].input,
						      tvs[i].input_size);
	      assert (res == CKR_OK);

	      dest[0] += k;
	      res = ncr_symm_signature_verify_final (sess, dest,
						     tvs[i].output_size);
	      if (k == 0)
		assert (res == CKR_OK);
	      else
		assert (res == CKR_SIGNATURE_INVALID);
	    }
	}

      res = ncr_symm_key_destroy (key);
      assert (res == CKR_OK);

      res = ncr_symm_signature_free (sess);
      assert (res == CKR_OK);
    }

  /* Test init + {sign,verify} with a random key. */
  for (i = 0; i < G_N_ELEMENTS (tvs); i++)
    {
      res = ncr_symm_signature_alloc (&sess, tvs[i].mech);
      assert (res == CKR_OK);

      res = ncr_symm_key_generate (&key, CKM_GENERIC_SECRET_KEY_GEN, true,
				   tvs[i].key_size);
      assert (res == CKR_OK);

      for (j = 0; j < 2; j++)
	{
	  res = ncr_symm_signature_sign_init (sess, key);
	  assert (res == CKR_OK);

	  dest_size = sizeof (dest);
	  res = ncr_symm_signature_sign (sess, dest, &dest_size,
					 tvs[i].input, tvs[i].input_size);
	  assert (res == CKR_OK);
	  assert (dest_size == tvs[i].output_size);

	  for (k = 0; k < 2; k++)
	    {
	      res = ncr_symm_signature_verify_init (sess, key);
	      assert (res == CKR_OK);

	      dest[0] += k;
	      res = ncr_symm_signature_verify (sess, dest, tvs[i].output_size,
					       tvs[i].input, tvs[i].input_size);
	      if (k == 0)
		assert (res == CKR_OK);
	      else
		assert (res == CKR_SIGNATURE_INVALID);
	    }
	}

      res = ncr_symm_key_destroy (key);
      assert (res == CKR_OK);

      res = ncr_symm_signature_free (sess);
      assert (res == CKR_OK);
    }

  /* Close the implicit reference, primarily to shut up valgrind. */
  res = ncr_close ();
  assert (res == CKR_OK);

  return EXIT_SUCCESS;
}
