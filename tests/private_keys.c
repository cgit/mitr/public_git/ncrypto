/* ncr_private_key_* tests.

Copyright 2010 Red Hat, Inc.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Red Hat author: Miloslav Trmač <mitr@redhat.com> */

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <ncrypto/ncrypto.h>

static const struct ncr_mpi import_mpis[NCR_RSA_PRIVATE_NUM_MPIS] =
  {
#define INT(NAME, VAL)						\
    [NCR_RSA_PRIVATE_MPI_##NAME] = { (void *)(VAL), sizeof (VAL) - 1 }

    INT (MODULUS,
	 "\xB8\xC7\x54\x15\x90\xCF\x91\x7A\xF3\x4C\x45\x53\xC2\x0A\xDA\x84\x4C\x09\x48\x10\x06\x41\xC5\x97\x57\x02\xDA\x0E\x7E\x64\x46\xBD\xC6\x75\x42\xCD\x32\x23\x0C\xEC\x2B\x1C\x60\x03\x68\x1E\x4F\x28\x78\xD8\xB0\xC1\xAC\xA7\x21\xE4\x15\x74\x65\x16\x1C\x59\xC8\x85"),
    INT (PUBLIC_EXPONENT, "\x01\x00\x01"),
    INT (PRIVATE_EXPONENT, "\x7B\xD6\xC3\xC8\xEC\x53\xE1\x09\xC9\x13\xDE\x06\xE3\xAE\xC8\x83\x10\x3E\xCC\x38\x49\x29\x3D\x97\x4F\x6E\x8E\xDC\x55\xE3\x38\xF1\x03\xEB\xC1\x09\x80\x16\xB8\x9F\xE1\xC0\x21\x77\xD4\xEE\xF7\x30\xD1\x85\x2B\x1F\x4F\xFE\xD1\x01\xCD\x35\x78\x4A\x97\x6F\x38\x65"),
    INT (PRIME_1, "\xF5\x7E\xFA\xED\xE0\xEC\x9C\x4E\x6F\xDF\xED\x64\x58\xEC\x18\xA9\x8E\x60\x2E\x49\x7E\xDF\x8E\xCF\x9F\xA5\x4A\x32\xA3\x27\x7E\x1B"),
    INT (PRIME_2, "\xC0\xAF\x49\x15\x49\x8D\x88\xFB\x28\x7D\x33\x25\x07\x37\xE0\x99\x2C\xA8\x6D\x46\x4F\x7D\x7D\x6E\x01\x95\x6B\x2B\x18\x1B\xBD\xDF"),
    INT (EXPONENT_1, "\xC3\x34\x0F\xB4\xBC\x87\x87\x95\xFA\xF1\x14\x63\x19\x2D\xCA\x42\x70\x5A\x5C\x13\xC6\x95\x5E\x8A\x0B\x08\x34\x22\x65\x87\x0E\x87"),
    INT (EXPONENT_2, "\x27\x3B\x89\x85\xEC\x14\x05\x70\x1E\x2E\x5F\xDB\x8A\x3C\xB6\x5E\x79\xD9\x51\x66\x9F\x88\xCD\xA1\x38\x71\x54\x00\xD2\x47\xD3\xC1"),
    INT (COEFFICIENT, "\x39\x09\x3A\x43\xCF\xE3\x65\x63\x2F\x5F\x11\xED\x2C\x42\x88\xEF\xCA\x26\x1E\x08\x96\xCF\x0A\x8F\xCB\x88\x45\x50\xEF\x6A\x38\x07")
#undef INT
  };

static void
log_silent (const gchar *log_domain, GLogLevelFlags log_level,
	    const gchar *message, gpointer user_data)
{
  (void)log_domain;
  (void)log_level;
  (void)message;
  (void)user_data;
}

static void
check_set_sentitive_failure (struct ncr_private_key *key)
{
  uint8_t dest[4096];
  size_t dest_size;
  CK_RV res;

  /* Extraction of a sensitive value is a programming error, so we complain to
     stderr.  Hide this in the test output. */

  g_log_set_default_handler (log_silent, NULL);

  dest_size = sizeof (dest);
  res = ncr_private_key_export (key, dest, &dest_size);
  assert (res == CKR_ATTRIBUTE_SENSITIVE);

  g_log_set_default_handler (g_log_default_handler, NULL);
}

int
main (void)
{
  uint8_t dest[4096];
  size_t dest_size;
  struct ncr_public_key *public;
  struct ncr_private_key *key;
  CK_RV res;

  /* Test handling of loaded, non-sensitive keys. */
  res = ncr_private_key_create_rsa (&key, false, import_mpis);
  assert (res == CKR_OK);

  dest_size = sizeof (dest);
  res = ncr_private_key_export (key, dest, &dest_size);
  assert (res == CKR_OK);

  res = ncr_private_key_set_sensitive (key);
  assert (res == CKR_OK);

  res = ncr_private_key_set_sensitive (key);
  assert (res == CKR_OK);

  check_set_sentitive_failure (key);

  res = ncr_private_key_destroy (key);
  assert (res == CKR_OK);


  /* Test handling of loaded, sensitive keys. */
  res = ncr_private_key_create_rsa (&key, true, import_mpis);
  assert (res == CKR_OK);

  check_set_sentitive_failure (key);

  res = ncr_private_key_destroy (key);
  assert (res == CKR_OK);


  /* Test handling of generated, non-sensitive keys. */
  res = ncr_key_pair_generate_rsa (&public, &key, CKM_RSA_PKCS_KEY_PAIR_GEN,
				   false, 1024, NULL);
  assert (res == CKR_OK);
  res = ncr_public_key_destroy (public);
  assert (res == CKR_OK);

  dest_size = sizeof (dest);
  res = ncr_private_key_export (key, dest, &dest_size);
  assert (res == CKR_OK);

  res = ncr_private_key_set_sensitive (key);
  assert (res == CKR_OK);

  res = ncr_private_key_set_sensitive (key);
  assert (res == CKR_OK);

  check_set_sentitive_failure (key);

  res = ncr_private_key_destroy (key);
  assert (res == CKR_OK);


  /* Test handling of generated, sensitive keys. */
  res = ncr_key_pair_generate_rsa (&public, &key, CKM_RSA_PKCS_KEY_PAIR_GEN,
				   true, 1024, NULL);
  assert (res == CKR_OK);
  res = ncr_public_key_destroy (public);
  assert (res == CKR_OK);

  check_set_sentitive_failure (key);

  res = ncr_private_key_destroy (key);
  assert (res == CKR_OK);

  /* Close the implicit reference, primarily to shut up valgrind. */
  res = ncr_close ();
  assert (res == CKR_OK);

  return EXIT_SUCCESS;
}
