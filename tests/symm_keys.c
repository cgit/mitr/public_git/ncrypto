/* ncr_symm_key_* tests.

Copyright 2010 Red Hat, Inc.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Red Hat author: Miloslav Trmač <mitr@redhat.com> */

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <ncrypto/ncrypto.h>

static void
log_silent (const gchar *log_domain, GLogLevelFlags log_level,
	    const gchar *message, gpointer user_data)
{
  (void)log_domain;
  (void)log_level;
  (void)message;
  (void)user_data;
}

static void
check_set_sentitive_failure (struct ncr_symm_key *key)
{
  uint8_t dest[256];
  size_t dest_size;
  CK_RV res;

  /* Extraction of a sensitive value is a programming error, so we complain to
     stderr.  Hide this in the test output. */

  g_log_set_default_handler (log_silent, NULL);

  dest_size = sizeof (dest);
  res = ncr_symm_key_export (key, dest, &dest_size);
  assert (res == CKR_ATTRIBUTE_SENSITIVE);

  g_log_set_default_handler (g_log_default_handler, NULL);
}

int
main (void)
{
  static const uint8_t input[32]
    = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F";

  uint8_t dest[256];
  size_t dest_size;
  struct ncr_symm_key *key;
  CK_RV res;

  /* Test handling of loaded, non-sensitive keys. */
  res = ncr_symm_key_create (&key, CKK_AES, false, input, sizeof (input));
  assert (res == CKR_OK);

  dest_size = sizeof (dest);
  res = ncr_symm_key_export (key, dest, &dest_size);
  assert (res == CKR_OK);
  assert (dest_size == sizeof (input));
  assert (memcmp (dest, input, dest_size) == 0);

  res = ncr_symm_key_set_sensitive (key);
  assert (res == CKR_OK);

  res = ncr_symm_key_set_sensitive (key);
  assert (res == CKR_OK);

  check_set_sentitive_failure (key);

  res = ncr_symm_key_destroy (key);
  assert (res == CKR_OK);


  /* Test handling of loaded, sensitive keys. */
  res = ncr_symm_key_create (&key, CKK_AES, true, input, sizeof (input));
  assert (res == CKR_OK);

  check_set_sentitive_failure (key);

  res = ncr_symm_key_destroy (key);
  assert (res == CKR_OK);


  /* Test handling of generated, non-sensitive keys. */
  res = ncr_symm_key_generate (&key, CKM_AES_KEY_GEN, false, sizeof (input));
  assert (res == CKR_OK);

  dest_size = sizeof (dest);
  res = ncr_symm_key_export (key, dest, &dest_size);
  assert (res == CKR_OK);
  assert (dest_size == sizeof (input));

  res = ncr_symm_key_set_sensitive (key);
  assert (res == CKR_OK);

  res = ncr_symm_key_set_sensitive (key);
  assert (res == CKR_OK);

  check_set_sentitive_failure (key);

  res = ncr_symm_key_destroy (key);
  assert (res == CKR_OK);


  /* Test handling of generated, sensitive keys. */
  res = ncr_symm_key_generate (&key, CKM_AES_KEY_GEN, true, sizeof (input));
  assert (res == CKR_OK);

  check_set_sentitive_failure (key);

  res = ncr_symm_key_destroy (key);
  assert (res == CKR_OK);

  /* Close the implicit reference, primarily to shut up valgrind. */
  res = ncr_close ();
  assert (res == CKR_OK);

  return EXIT_SUCCESS;
}
