/* libncrypto interface.

Copyright 2010 Red Hat, Inc.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Red Hat author: Miloslav Trmač <mitr@redhat.com> */

#ifndef NCRYPTO_H__
#define NCRYPTO_H__

#include <stddef.h>

#include <pkcs11.h>

/* In general, semantics aspects that are not documented here are same as in
   PCKS#11.

   Simultaneous references to the same object from multiple threads (except for
   simultaneous read accesses to a key) cause undefined behavior. */

 /* Global state */

/* A program should pair calls of ncr_open () with ncr_close ().  In absence
   of ncr_open (), the library will be initialized automatically, and
   ncr_close () will deinitialize it.  */

CK_RV ncr_open (void);
CK_RV ncr_close (void);

 /* Random numbers */

CK_RV ncr_get_random_bytes (void *dest, size_t size);

 /* Symmetric keys */

struct ncr_symm_key;

/* "Sensitive" corresponds to CKA_SENSITIVE. */
CK_RV ncr_symm_key_create (struct ncr_symm_key **key, CK_KEY_TYPE type,
			   _Bool sensitive, const void *value,
			   size_t value_size);
CK_RV ncr_symm_key_generate (struct ncr_symm_key **key, CK_MECHANISM_TYPE mech,
			     _Bool sensitive, size_t value_size);
CK_RV ncr_symm_key_set_sensitive (struct ncr_symm_key *key);
CK_RV ncr_symm_key_export (struct ncr_symm_key *key, void *dest,
			   size_t *dest_size_ptr);
CK_RV ncr_symm_key_destroy (struct ncr_symm_key *key);

 /* Asymmetric keys */

struct ncr_public_key;
struct ncr_private_key;

/* The X.509v3 subjectPublicKeyInfo structure is used for public keys. */
CK_RV ncr_public_key_create (struct ncr_public_key **key, const void *der,
			     size_t der_size);
CK_RV ncr_public_key_export (struct ncr_public_key *key, void *dest,
			     size_t *dest_size_ptr);
CK_RV ncr_public_key_destroy (struct ncr_public_key *key);

/* The PKCS#8 privateKeyInfo structure is used for private keys. "Sensitive"
   corresponds to CKA_SENSITIVE. */
/* Use algorithm-specific ncr_private_key_create_* for key creation. */
CK_RV ncr_private_key_set_sensitive (struct ncr_private_key *key);
CK_RV ncr_private_key_export (struct ncr_private_key *key, void *dest,
			      size_t *dest_size_ptr);
CK_RV ncr_private_key_destroy (struct ncr_private_key *key);

 /* Multi-precision integers */

/* This is used to avoid e.g. 8 separate parameters for RSA private key
   passing. */
struct ncr_mpi
{
  void *data;
  size_t size;
};

 /* RSA keys */

enum
  {
    NCR_RSA_PUBLIC_MPI_MODULUS,
    NCR_RSA_PUBLIC_MPI_PUBLIC_EXPONENT,
    NCR_RSA_PUBLIC_NUM_MPIS
  };

enum
  {
    NCR_RSA_PRIVATE_MPI_MODULUS,
    NCR_RSA_PRIVATE_MPI_PUBLIC_EXPONENT,
    NCR_RSA_PRIVATE_MPI_PRIVATE_EXPONENT,
    NCR_RSA_PRIVATE_MPI_PRIME_1,
    NCR_RSA_PRIVATE_MPI_PRIME_2,
    NCR_RSA_PRIVATE_MPI_EXPONENT_1,
    NCR_RSA_PRIVATE_MPI_EXPONENT_2,
    NCR_RSA_PRIVATE_MPI_COEFFICIENT,
    NCR_RSA_PRIVATE_NUM_MPIS
  };

CK_RV ncr_public_key_create_rsa (struct ncr_public_key **key,
				 const struct ncr_mpi
				 mpis[static NCR_RSA_PUBLIC_NUM_MPIS]);
CK_RV ncr_public_key_export_rsa (struct ncr_public_key *key,
				 struct ncr_mpi
				 mpis [static NCR_RSA_PUBLIC_NUM_MPIS]);
CK_RV ncr_private_key_create_rsa (struct ncr_private_key **key, _Bool sensitive,
				  const struct ncr_mpi
				  mpis[static NCR_RSA_PRIVATE_NUM_MPIS]);
CK_RV ncr_private_key_export_rsa (struct ncr_private_key *key,
				  struct ncr_mpi
				  mpis[static NCR_RSA_PRIVATE_NUM_MPIS]);
CK_RV ncr_key_pair_generate_rsa (struct ncr_public_key **public_key,
				 struct ncr_private_key **private_key,
				 CK_MECHANISM_TYPE mech, _Bool sensitive,
				 CK_ULONG modulus_bits,
				 const struct ncr_mpi *public_exponent);

 /* DSA keys */

enum
  {
    NCR_DSA_PUBLIC_MPI_PRIME,
    NCR_DSA_PUBLIC_MPI_SUBPRIME,
    NCR_DSA_PUBLIC_MPI_BASE,
    NCR_DSA_PUBLIC_MPI_VALUE,
    NCR_DSA_PUBLIC_NUM_MPIS
  };

enum
  {
    NCR_DSA_PRIVATE_MPI_PRIME,
    NCR_DSA_PRIVATE_MPI_SUBPRIME,
    NCR_DSA_PRIVATE_MPI_BASE,
    NCR_DSA_PRIVATE_MPI_VALUE,
    NCR_DSA_PRIVATE_NUM_MPIS
  };

enum
  {
    NCR_DSA_GEN_MPI_PRIME,
    NCR_DSA_GEN_MPI_SUBPRIME,
    NCR_DSA_GEN_MPI_BASE,
    NCR_DSA_GEN_NUM_MPIS
  };

CK_RV ncr_public_key_create_dsa (struct ncr_public_key **key,
				 const struct ncr_mpi
				 mpis[static NCR_DSA_PUBLIC_NUM_MPIS]);
CK_RV ncr_public_key_export_dsa (struct ncr_public_key *key,
				 struct ncr_mpi
				 mpis [static NCR_DSA_PUBLIC_NUM_MPIS]);
CK_RV ncr_private_key_create_dsa (struct ncr_private_key **key, _Bool sensitive,
				  const struct ncr_mpi
				  mpis[static NCR_DSA_PRIVATE_NUM_MPIS],
				  const struct ncr_mpi *public_value);
CK_RV ncr_private_key_export_dsa (struct ncr_private_key *key,
				  struct ncr_mpi
				  mpis[static NCR_DSA_PRIVATE_NUM_MPIS]);
CK_RV ncr_key_pair_generate_dsa (struct ncr_public_key **public_key,
				 struct ncr_private_key **private_key,
				 CK_MECHANISM_TYPE mech, _Bool sensitive,
				 const struct ncr_mpi
				 mpis[static NCR_DSA_GEN_NUM_MPIS]);

 /* Asymmetric operations */

CK_RV ncr_public_key_encrypt (CK_MECHANISM_TYPE mech,
			      struct ncr_public_key *key, void *dest,
			      size_t *dest_size_ptr, const void *src,
			      size_t src_size);
CK_RV ncr_private_key_decrypt (CK_MECHANISM_TYPE mech,
			       struct ncr_private_key *key, void *dest,
			       size_t *dest_size_ptr, const void *src,
			       size_t src_size);

/* FIXME? Add multipart sign/verify with included hashing - not currently
   supported by NSS. */
CK_RV ncr_private_key_sign (CK_MECHANISM_TYPE mech,
			    struct ncr_private_key *key, void *dest,
			    size_t *dest_size_ptr, const void *src,
			    size_t src_size);
CK_RV ncr_public_key_verify (CK_MECHANISM_TYPE mech,
			     struct ncr_public_key *key, const void *signature,
			     size_t signature_size, const void *src,
			     size_t src_size);

 /* Digests */

struct ncr_digest_session;

/* Session lifetime management. */
CK_RV ncr_digest_alloc (struct ncr_digest_session **sess,
			CK_MECHANISM_TYPE mech);
CK_RV ncr_digest_free (struct ncr_digest_session *sess);
CK_RV ncr_digest_clone (struct ncr_digest_session **clone,
			struct ncr_digest_session *sess);
/* Use either ncr_digest_{init,update*,final} (), or
   ncr_{digest_init,digest} ().  After finishing such a call sequence, a new
   sequence can be started within the same session. */
CK_RV ncr_digest_init (struct ncr_digest_session *sess);
CK_RV ncr_digest_update (struct ncr_digest_session *sess, const void *data,
			 size_t size);
CK_RV ncr_digest_final (struct ncr_digest_session *sess, void *dest,
			size_t *size_ptr);
CK_RV ncr_digest (struct ncr_digest_session *sess, void *dest,
		  size_t *dest_size_ptr, const void *data, size_t data_size);

/* You'll get better performance by keeping a long-term digest session than
   repeatedly calling this function. */
CK_RV ncr_digest_standalone (CK_MECHANISM_TYPE mech, void *dest,
			     size_t *dest_size_ptr, const void *data,
			     size_t data_size);

 /* Symmetric encryption */

struct ncr_symm_cipher_session;

/* Note that for *_ECB and *_CBC, the input must be block-aligned. */

/* Session lifetime management. */
CK_RV ncr_symm_cipher_alloc (struct ncr_symm_cipher_session **sess,
			     CK_MECHANISM_TYPE mech);
CK_RV ncr_symm_cipher_free (struct ncr_symm_cipher_session *sess);
CK_RV ncr_symm_cipher_change_iv (struct ncr_symm_cipher_session *sess,
				 const void *iv, size_t iv_size);
/* Use either ncr_symm_cipher_encrypt_{init,update,final} (), or
   ncr_symm_cipher_{encrypt_init,encrypt} ().  After finishing such a call
   sequence, a new sequence can be started within the same session.  Same for
   decryption sequences. */
CK_RV ncr_symm_cipher_encrypt_init (struct ncr_symm_cipher_session *sess,
				    struct ncr_symm_key *key,
				    const void *param, size_t param_size);
CK_RV ncr_symm_cipher_encrypt_update (struct ncr_symm_cipher_session *sess,
				      void *dest, size_t *dest_size_ptr,
				      const void *src, size_t src_size);
CK_RV ncr_symm_cipher_encrypt_final (struct ncr_symm_cipher_session *sess,
				     void *dest, size_t *dest_size_ptr,
				     const void *src, size_t src_size);
CK_RV ncr_symm_cipher_encrypt (struct ncr_symm_cipher_session *sess, void *dest,
			       size_t *dest_size_ptr, const void *src,
			       size_t src_size);
CK_RV ncr_symm_cipher_decrypt_init (struct ncr_symm_cipher_session *sess,
				    struct ncr_symm_key *key,
				    const void *param, size_t param_size);
CK_RV ncr_symm_cipher_decrypt_update (struct ncr_symm_cipher_session *sess,
				      void *dest, size_t *dest_size_ptr,
				      const void *src, size_t src_size);
CK_RV ncr_symm_cipher_decrypt_final (struct ncr_symm_cipher_session *sess,
				     void *dest, size_t *dest_size_ptr,
				     const void *src, size_t src_size);
CK_RV ncr_symm_cipher_decrypt (struct ncr_symm_cipher_session *sess, void *dest,
			       size_t *dest_size_ptr, const void *src,
			       size_t src_size);

 /* Symmetric signatures */

struct ncr_symm_signature_session;

/* Session lifetime management. */
CK_RV ncr_symm_signature_alloc (struct ncr_symm_signature_session **sess,
				CK_MECHANISM_TYPE mech);
CK_RV ncr_symm_signature_free (struct ncr_symm_signature_session *sess);
CK_RV ncr_symm_signature_clone (struct ncr_symm_signature_session **clone,
				struct ncr_symm_signature_session *sess);
/* Use either ncr_symm_signature_sign_{init,update,final} (), or
   ncr_symm_signature_{sign_init,sign} ().  After finishing such a call
   sequence, a new sequence can be started within the same session.  Same for
   verification sequences.

   Symmetric signature mechanisms  tend to use keys of type
   CKK_GENERIC_SECRET. */
CK_RV ncr_symm_signature_sign_init (struct ncr_symm_signature_session *sess,
				    struct ncr_symm_key *key);
CK_RV ncr_symm_signature_sign_update (struct ncr_symm_signature_session *sess,
				      const void *data, size_t size);
CK_RV ncr_symm_signature_sign_final (struct ncr_symm_signature_session *sess,
				     void *dest, size_t *size_ptr);
CK_RV ncr_symm_signature_sign (struct ncr_symm_signature_session *sess,
			       void *dest, size_t *dest_size_ptr,
			       const void *data, size_t data_size);
CK_RV ncr_symm_signature_verify_init (struct ncr_symm_signature_session *sess,
				      struct ncr_symm_key *key);
CK_RV ncr_symm_signature_verify_update (struct ncr_symm_signature_session *sess,
					const void *data, size_t size);
CK_RV ncr_symm_signature_verify_final (struct ncr_symm_signature_session *sess,
				       const void *signature, size_t size);
CK_RV ncr_symm_signature_verify (struct ncr_symm_signature_session *sess,
				 const void *signature, size_t signature_size,
				 const void *data, size_t data_size);

#endif
